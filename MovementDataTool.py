import DataExtractor
import MovementDataFileManager
import ML_InputGenerator
from GlobalConfig import global_config
import strictyaml

class MovementDataTool:
    
    data_extractor = DataExtractor.DataExtractor()
    file_manager = MovementDataFileManager.MovementDataFileManager()
    ml_input_generator = ML_InputGenerator.ML_InputGenerator()
    
    def basic_flow(self):
        files = self.file_manager.get_raw_data_file_paths()
        print(f"got files, opneing file: {files[0][0]}")
        timestamps = self.data_extractor.extract_timestamps(files[0][1])
        measurepoints = self.data_extractor.extract_measure_points(files[0][0])
        recognisable_segments = self.data_extractor.segment_measure_points(measurepoints, timestamps, 60)
        unrelated_segments = self.data_extractor.segment_unrelated_data(measurepoints, timestamps, 60)
        # self.file_manager.save_cropped_recogniseable_data(recognisable_segments, global_config.path.preparing.cropped_recognisable_movement)
        # self.file_manager.save_cropped_recogniseable_data(unrelated_segments, global_config.path.preparing.cropped_unrelated_movement)

        recognisable_dataset = self.ml_input_generator.create_dataset(recognisable_segments)
        unrelated_dataset = self.ml_input_generator.create_dataset(unrelated_segments)
        print(f"Files in dataset for recognisable movement: \n- train: {len(recognisable_dataset[0])} \n- validation: {len(recognisable_dataset[1])} \n- test: {len(recognisable_dataset[2])}")
        self.file_manager.save_recognisable_dataset(recognisable_dataset)
        self.file_manager.save_unrelated_dataset(unrelated_dataset)

if __name__ == '__main__':
    movement_data_tool = MovementDataTool()
    movement_data_tool.basic_flow()