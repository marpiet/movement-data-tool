import pandas as pd
from GlobalConfig import global_config
import random

class ML_InputGenerator:

    def create_dataset(self, dataframes: [pd.DataFrame]):
        (train_size, validation_size, test_size) = self.__retrieve_data_sizing(len(dataframes))
        copied_dataframes = dataframes.copy()
        random.shuffle(copied_dataframes)

        train_dataset = dataframes[0:train_size]
        validation_dataset = dataframes[train_size:(train_size + validation_size)]
        test_dataset = dataframes[train_size + validation_size:train_size + validation_size + test_size]

        return (train_dataset, validation_dataset, test_dataset)

    def __retrieve_data_sizing(self, dataframe_length: int) -> (float, float, float):
        if dataframe_length < 3:
            print("Data frame is too small. Min. 3 entries needed!")

        train_percentile = global_config.meta.ml_data.sizing.train
        validation_percentile = global_config.meta.ml_data.sizing.validation
        test_percentile = global_config.meta.ml_data.sizing.test

        total_size: float = train_percentile + validation_percentile + test_percentile
        if (total_size) != 1:
            print(f"Sizing should be in total 1, currently it is: {total_size}")
            raise Exception()

        train_size = dataframe_length * train_percentile
        validation_size = dataframe_length * validation_percentile
        test_size = dataframe_length * test_percentile

        if (train_size + validation_size + test_size) != dataframe_length:
            print("Warning: DataFrame not properly covered!")

        return (int(train_size), int(validation_size), int(test_size))
