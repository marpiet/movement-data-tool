import pandas as pd
import glob
from pathlib import Path
import os.path
from GlobalConfig import global_config
import uuid

class MovementDataFileManager:

    def get_raw_data_file_paths(self):
        measure_point_paths = sorted(glob.glob(f'{global_config.path.preparing.raw_movement}*{global_config.meta.measure_points.postfix}.{global_config.meta.raw_input_filetype}'))
        data_tuples = []
        
        for measure_point_path in measure_point_paths:
            time_stamp_path = measure_point_path.replace(global_config.meta.measure_points.postfix, global_config.meta.timestamps.postfix, 1)
            if os.path.isfile(time_stamp_path):
                data_tuples.append((measure_point_path, time_stamp_path))
            else:
                print(f"Error: timeStamp file does not exist for file: {measure_point_path}")
        
        return data_tuples

    def segment_recognisable_data(self, measure_points_file_path: str, time_points_file_path: str):
        if not os.path.isfile(measure_points_file_path):
            print(f"Error: measurePoints file is missing: {measure_points_file_path}")
            return
        if not os.path.isfile(time_points_file_path):
            print(f"Error: timePoints file is missing: {time_points_file_path}")
            return

    def save_cropped_recogniseable_data(self, cropped_data, directory_path: str):
        for data_entry in cropped_data:
            file_name: str = data_entry.iloc[0].name.strftime(global_config.meta.datetime.format)
            filepath_to_save = directory_path + file_name + f".{global_config.meta.raw_input_filetype}"
            data_entry.to_csv(filepath_to_save)

    def save_recognisable_dataset(self, dataset: (pd.DataFrame, pd.DataFrame, pd.DataFrame)):
        (train_dataset, validation_dataset, test_dataset) = dataset
        self.__save_dataset(train_dataset, global_config.path.ml_input.training.recognisable_movement)
        self.__save_dataset(validation_dataset, global_config.path.ml_input.validation.recognisable_movement)
        self.__save_dataset(test_dataset, global_config.path.ml_input.test.recognisable_movement)

    def save_unrelated_dataset(self, dataset: (pd.DataFrame, pd.DataFrame, pd.DataFrame)):
        (train_dataset, validation_dataset, test_dataset) = dataset
        self.__save_dataset(train_dataset, global_config.path.ml_input.training.unrelated_movement)
        self.__save_dataset(validation_dataset, global_config.path.ml_input.validation.unrelated_movement)
        self.__save_dataset(test_dataset, global_config.path.ml_input.test.unrelated_movement)
    
    def __save_dataset(self, dataset: pd.DataFrame, filepath_to_save: str):
        number_of_files: int = len([name for name in os.listdir(filepath_to_save) if os.path.isfile(os.path.join(filepath_to_save, name))])
        for entry in dataset:
            entry.to_csv(filepath_to_save + f"{number_of_files}-" + str(uuid.uuid4()) + "." + global_config.meta.ml_input_filetype)
            number_of_files += 1

if __name__ == '__main__':
    movement_data_file_manager = MovementDataFileManager()
    test = movement_data_file_manager.get_raw_data_file_paths()
    print("test")
    print(f"TestiTest: {test}")