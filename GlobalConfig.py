from strictyaml import load, Map, Str, Int, Float, YAMLError

class GlobalConfig:

    PATH = "path"
    META = "meta"

    def __init__(self, config_file_path: str = "config.yaml"):
        self.load_configuration(config_file_path)

    def load_configuration(self, config_file_path: str = "config.yaml"):
        with open(config_file_path, "r") as config_file:
            schema = self.__retrieve_yaml_schema()

            try:
                self.configuration: dict = load(config_file.read(), schema).data
            except YAMLError as error:
                print(error)
                raise error

            self.path = self.Path(self.configuration[self.PATH])
            self.meta = self.Meta(self.configuration[self.META])

    def __retrieve_yaml_schema(self):
        return Map({
            self.PATH: Map({
                self.Path.PREPARING: Map({
                    self.Path.Preparing.RAW_MOVEMENT: Str(),
                    self.Path.Preparing.CROPPED_RECOGNISABLE_MOVEMENT: Str(),
                    self.Path.Preparing.CROPPED_UNRELATED_MOVEMENT: Str(),
                }),
                self.Path.ML_INPUT: Map({
                    self.Path.ML_Input.TRAINING_ROOT_DIRECTORY: Map({
                        self.Path.ML_Input.Training.RECOGNISABLE_MOVEMENT: Str(),
                        self.Path.ML_Input.Training.UNRELATED_MOVEMENT: Str(),
                    }),
                    self.Path.ML_Input.VALIDATION_ROOT_DIRECTORY: Map({
                        self.Path.ML_Input.Validation.RECOGNISABLE_MOVEMENT: Str(),
                        self.Path.ML_Input.Validation.UNRELATED_MOVEMENT: Str(),
                    }),
                    self.Path.ML_Input.TEST_ROOT_DIRECTORY: Map({
                        self.Path.ML_Input.Test.RECOGNISABLE_MOVEMENT: Str(),
                        self.Path.ML_Input.Test.UNRELATED_MOVEMENT: Str(),
                    })
                })
            }),
            self.META: Map({
                self.Meta.MOVEMENT_SEGMENT: Map({
                    self.Meta.MovementSegment.SECONDS: Int(),
                    self.Meta.MovementSegment.DATA_POINTS_PER_SECOND: Int(),
                }),
                self.Meta.RAW_INPUT_FILETYPE: Str(),
                self.Meta.ML_INPUT_FILETYPE: Str(),
                self.Meta.DATETIME: Map({
                    self.Meta.Datetime.FORMAT: Str(),
                    self.Meta.Datetime.UNIT: Str(),
                }),
                self.Meta.MEASURE_POINTS: Map({
                    self.Meta.MeasurePoints.POSTFIX: Str(),
                }),
                self.Meta.TIMESTAMPS: Map({
                    self.Meta.Timestamps.POSTFIX: Str(),
                    self.Meta.Timestamps.DATE_HEADER: Str(),
                    self.Meta.Timestamps.EVENT_HEADER: Str(),
                    self.Meta.Timestamps.BEGIN_TAG: Str(),
                    self.Meta.Timestamps.END_TAG: Str(),
                }),
                self.Meta.ML_DATA: Map({
                    self.Meta.ML_Data.SIZING: Map({
                        self.Meta.ML_Data.Sizing.TRAIN: Float(),
                        self.Meta.ML_Data.Sizing.VALIDATION: Float(),
                        self.Meta.ML_Data.Sizing.TEST: Float(),
                    }),
                }),
            }),
        })

    class Path:

        PREPARING = "preparing"
        ML_INPUT = "ml-input"

        def __init__(self, paths_dict: dict):
            self.preparing = self.Preparing(paths_dict[self.PREPARING])
            self.ml_input = self.ML_Input(paths_dict[self.ML_INPUT])

        class Preparing:

            RAW_MOVEMENT = "raw-movement"
            CROPPED_RECOGNISABLE_MOVEMENT = "cropped-recognisable-movement"
            CROPPED_UNRELATED_MOVEMENT = "cropped-unrelated-movement"

            def __init__(self, preparing_dict: dict):
                self.raw_movement: str = preparing_dict[self.RAW_MOVEMENT]
                self.cropped_recognisable_movement: str = preparing_dict[self.CROPPED_RECOGNISABLE_MOVEMENT]
                self.cropped_unrelated_movement: str = preparing_dict[self.CROPPED_UNRELATED_MOVEMENT]

        class ML_Input:

            TRAINING_ROOT_DIRECTORY = "training"
            VALIDATION_ROOT_DIRECTORY = "validation"
            TEST_ROOT_DIRECTORY = "test"

            def __init__(self, ml_input_dict: dict):
                self.training = self.Training(ml_input_dict[self.TRAINING_ROOT_DIRECTORY])
                self.validation = self.Validation(ml_input_dict[self.VALIDATION_ROOT_DIRECTORY])
                self.test = self.Test(ml_input_dict[self.TEST_ROOT_DIRECTORY])


            class Training:

                RECOGNISABLE_MOVEMENT = "recognisable-movement"
                UNRELATED_MOVEMENT = "unrelated-movement"

                def __init__(self, training_dict: dict):
                    self.recognisable_movement: str = training_dict[self.RECOGNISABLE_MOVEMENT]
                    self.unrelated_movement: str = training_dict[self.UNRELATED_MOVEMENT]

            class Validation:

                RECOGNISABLE_MOVEMENT = "recognisable-movement"
                UNRELATED_MOVEMENT = "unrelated-movement"

                def __init__(self, validation_dict: dict):
                    self.recognisable_movement: str = validation_dict[self.RECOGNISABLE_MOVEMENT]
                    self.unrelated_movement: str = validation_dict[self.UNRELATED_MOVEMENT]

            class Test:

                RECOGNISABLE_MOVEMENT = "recognisable-movement"
                UNRELATED_MOVEMENT = "unrelated-movement"

                def __init__(self, test_dict: dict):
                    self.recognisable_movement: str = test_dict[self.RECOGNISABLE_MOVEMENT]
                    self.unrelated_movement: str = test_dict[self.UNRELATED_MOVEMENT]

    class Meta:

        MOVEMENT_SEGMENT = "movement-segment"
        RAW_INPUT_FILETYPE = "raw-input-filetype"
        ML_INPUT_FILETYPE = "ml-input-filetype"
        DATETIME = "datetime"
        MEASURE_POINTS = "measure-points"
        TIMESTAMPS = "timestamps"
        ML_DATA = "ml-data"


        def __init__(self, meta_dict: dict):
            self.movement_segment = self.MovementSegment(meta_dict[self.MOVEMENT_SEGMENT])
            self.raw_input_filetype = meta_dict[self.RAW_INPUT_FILETYPE]
            self.ml_input_filetype = meta_dict[self.ML_INPUT_FILETYPE]
            self.datetime = self.Datetime(meta_dict[self.DATETIME])
            self.measure_points = self.MeasurePoints(meta_dict[self.MEASURE_POINTS])
            self.timestamps = self.Timestamps(meta_dict[self.TIMESTAMPS])
            self.ml_data = self.ML_Data(meta_dict[self.ML_DATA])

        class MovementSegment:

            SECONDS = "seconds"
            DATA_POINTS_PER_SECOND = "data-points-per-second"

            def __init__(self, movement_segment_dict: dict):
                self.seconds: int = movement_segment_dict[self.SECONDS]
                self.data_points_per_second: int = movement_segment_dict[self.DATA_POINTS_PER_SECOND]

        class Datetime:
            
            FORMAT = "format"
            UNIT = "unit"

            def __init__(self, datetime_dict: dict):
                self.format: str = datetime_dict[self.FORMAT]
                self.unit: int = datetime_dict[self.UNIT]

        class MeasurePoints:

            POSTFIX = "postfix"

            def __init__(self, measurepoints_dict: dict):
                self.postfix: str = measurepoints_dict[self.POSTFIX]

        class Timestamps:

            POSTFIX = "postfix"
            DATE_HEADER = "date-header"
            EVENT_HEADER = "event-header"
            BEGIN_TAG = "begin-tag"
            END_TAG = "end-tag"

            def __init__(self, timetamps_dict: dict):
                self.postfix: str = timetamps_dict[self.POSTFIX]
                self.date_header: str = timetamps_dict[self.DATE_HEADER]
                self.event_header: str = timetamps_dict[self.EVENT_HEADER]
                self.begin_tag: str = timetamps_dict[self.BEGIN_TAG]
                self.end_tag: str = timetamps_dict[self.END_TAG]

        class ML_Data:

            SIZING = "sizing"

            def __init__(self, ml_data_dict: dict):
                self.sizing = self.Sizing(ml_data_dict[self.SIZING])

            class Sizing:

                TRAIN = "train"
                VALIDATION = "validation"
                TEST = "test"

                def __init__(self, sizing_dict: dict):
                    self.train: float = sizing_dict[self.TRAIN]
                    self.validation: float = sizing_dict[self.VALIDATION]
                    self.test: float = sizing_dict[self.TEST]

global_config = GlobalConfig()
