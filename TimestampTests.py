import unittest
import DataExtractor
import MovementDataFileManager
from GlobalConfig import global_config
import datetime

class TimestampTests(unittest.TestCase): 

    data_extractor = DataExtractor.DataExtractor()
    file_manager = MovementDataFileManager.MovementDataFileManager()        

    def test_timestamp_converting(self):
        files = self.file_manager.get_raw_data_file_paths()
        date = self.data_extractor.extract_timestamps(files[0][1])[0]
        self.assertEqual(date.timestamp(), 1606579448.606849)

    def test_segmentLength_handleBiasedBeginWithNegativeIndex(self):
        files = self.file_manager.get_raw_data_file_paths()
        measurepoints = self.data_extractor.extract_measure_points(files[0][0])
        timestamps = self.data_extractor.extract_timestamps(files[0][1])[:2]
        recognisable_segments = self.data_extractor.segment_measure_points(measurepoints, timestamps, 60)[0]
        self.assertEqual(len(recognisable_segments), global_config.meta.movement_segment.seconds * global_config.meta.movement_segment.data_points_per_second)

    def test_segmentLength_handleShortTimeInterval(self):
        files = self.file_manager.get_raw_data_file_paths()
        measurepoints = self.data_extractor.extract_measure_points(files[0][0])
        timestamps = self.data_extractor.extract_timestamps(files[0][1])[2:4]
        recognisable_segments = self.data_extractor.segment_measure_points(measurepoints, timestamps, 60)[0]
        self.assertEqual(len(recognisable_segments), global_config.meta.movement_segment.seconds * global_config.meta.movement_segment.data_points_per_second)

    def test_segmentLength_handleWholeFile(self):
        files = self.file_manager.get_raw_data_file_paths()
        measurepoints = self.data_extractor.extract_measure_points(files[0][0])
        timestamps = self.data_extractor.extract_timestamps(files[0][1])[4:6]
        recognisable_segments = self.data_extractor.segment_measure_points(measurepoints, timestamps, 60)[0]
        self.assertEqual(len(recognisable_segments), global_config.meta.movement_segment.seconds * global_config.meta.movement_segment.data_points_per_second)

    def test_segmentLength_handleBeginTagOnly(self):
        files = self.file_manager.get_raw_data_file_paths()
        measurepoints = self.data_extractor.extract_measure_points(files[0][0])
        timestamps = self.data_extractor.extract_timestamps(files[0][1])[6:8]
        recognisable_segments = self.data_extractor.segment_measure_points(measurepoints, timestamps, 60)[0]
        self.assertEqual(len(recognisable_segments), global_config.meta.movement_segment.seconds * global_config.meta.movement_segment.data_points_per_second)

    def test_segmentLength_handleWrongTagOrder(self):
        files = self.file_manager.get_raw_data_file_paths()
        measurepoints = self.data_extractor.extract_measure_points(files[0][0])
        timestamps = self.data_extractor.extract_timestamps(files[0][1])[8:10]
        recognisable_segments = self.data_extractor.segment_measure_points(measurepoints, timestamps, 60)[0]
        self.assertEqual(len(recognisable_segments), global_config.meta.movement_segment.seconds * global_config.meta.movement_segment.data_points_per_second)

if __name__ == '__main__':
    global_config.load_configuration("test_config.yaml")
    unittest.main()