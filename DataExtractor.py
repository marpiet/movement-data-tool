import pandas as pd
from GlobalConfig import global_config

class DataExtractor:

    def extract_timestamps(self, timestamps_filepath: str):
        print(f"opening csv: {timestamps_filepath}")
        df = pd.read_csv(timestamps_filepath, comment="#")
        df[global_config.meta.timestamps.date_header] = pd.to_datetime(df[global_config.meta.timestamps.date_header], unit=global_config.meta.datetime.unit)
        
        return df[global_config.meta.timestamps.date_header].tolist()

    def extract_measure_points(self, measurepoints_filepath):
        df = pd.read_csv(measurepoints_filepath, index_col=0, comment="#")
        df.index = pd.to_datetime(df.index, unit=global_config.meta.datetime.unit)
        return df

    def extract_measure_points_raw(self, measurepoints_filepath):
        df = pd.read_csv(measurepoints_filepath, index_col=0)
        df.index = pd.to_datetime(df.index, unit=global_config.meta.datetime.unit)
        return df

    def segment_measure_points(self, measurepoints, timestamps, segment_length):
        if len(timestamps) % 2 != 0:
            print("Invalid timestamps for measurepoints: Number of timestamps have to be even.")
            return
        if len(timestamps) == 0:
            print("No timestamps provided!")
            return
        if len(measurepoints) < segment_length:
            print("Segment length is greater than measurepoints length!")
            return
        segments = []

        for i in range(0, len(timestamps), 2):
            print(f"i: {i}")
            begin_timestamp = timestamps[i]
            end_timestamp = timestamps[i + 1]
            mid_timestamp = end_timestamp - (end_timestamp - begin_timestamp) / 2
            mid_measurepoint_index = measurepoints.index.get_loc(mid_timestamp, method='nearest')

            biased_begin_timestamp = int(mid_measurepoint_index - segment_length / 2)
            if biased_begin_timestamp < 0:
                biased_begin_timestamp = 0
                biased_end_timestamp = segment_length
            else:
                biased_end_timestamp = int(mid_measurepoint_index + segment_length / 2)
            
            segments.append(measurepoints[biased_begin_timestamp:biased_end_timestamp])

        return segments

    def segment_unrelated_data(self, measurepoints, timestamps, segment_length):
        if len(timestamps) % 2 != 0:
            print("Invalid timestamps for measurepoints: Number of timestamps have to be even.")
            return
        if len(timestamps) == 0:
            print("No timestamps provided!")
            return
        segments = []

        current_index = 0
        for i in range(0, len(timestamps), 2):
            begin_timestamp = timestamps[i]
            begin_measurepoint_index = measurepoints.index.get_loc(begin_timestamp, method='nearest')
            
            print(f"Current index {current_index}, begin_measurepoint_index {begin_measurepoint_index}, segment_length {segment_length}, len(measurepoints) {len(measurepoints)}")
            while current_index < begin_measurepoint_index & current_index + segment_length < len(measurepoints) - 1:
                segments.append(measurepoints[current_index: current_index + segment_length])
                current_index += segment_length
                
        last_timestamp = timestamps[len(timestamps)-1]
        current_index = measurepoints.index.get_loc(last_timestamp, method='nearest')

        while current_index + segment_length < len(measurepoints) - 1:
            segments.append(measurepoints[current_index: current_index + segment_length])
            current_index += segment_length

        return segments
